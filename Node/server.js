var express   = require( 'express' );
var amqp    = require( 'amqp' );
var bodyParser = require('body-parser');
var StatsD = require('node-statsd').StatsD,
    client = new StatsD({ host: 'stats-server', port: '8125', prefix: 'node.'});

var app = express();

app.use(bodyParser());

var conn = amqp.createConnection({ url: "amqp://elasticsearch:Summer88@172.21.200.61:5672" }, {
    reconnect: true, // Enable reconnection
    reconnectBackoffStrategy: 'linear',
    reconnectBackoffTime: 1000, // Try reconnect once a second
}); 

conn.on('ready', function () {

  var counter = 1;
  
  var exchange = conn.exchange('');
  console.log('Connected');

  var router = express.Router();

  router.route('/')
  .get(function(req, res){
    res.send('Success');
  });

  router.route('/events/:event_type/:event_id')
  .put(function( req, res ) {

    var start = process.hrtime();

    var payloadHeader = {
      "index": {
      "_index" : "seekedc",
      "_type" : req.params.event_type,
      "_id" : req.params.event_id  
      }
    };
                   
    var payloadBody = {
      "EventId": req.params.event_id,
      "Timestamp": new Date().toISOString(),
      "Type": req.params.event_type,
      "Data": {}
    };

    var payload = JSON.stringify(payloadHeader) + "\n" + JSON.stringify(payloadBody);


    exchange.publish("node", payload); 
    
    var rabbit = process.hrtime(start);

    res.json({"location":"/events/"+req.params.event_type+"/"+req.params.event_id});

    var api = process.hrtime(start);

    client.increment('api.counter', counter, 1/1000);
    client.timing('rabbit.time', rabbit[1] / 1000000);
    client.timing('api.time', api[1] / 1000000);

    counter++;

  });

  app.use('/', router);

});


app.listen(80);
console.log('Listening on port 80...');