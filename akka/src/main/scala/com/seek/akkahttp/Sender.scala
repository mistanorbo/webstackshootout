package com.seek.akkahttp

import akka.actor.{ActorLogging, Props, Actor}
import com.rabbitmq.client.Channel
import java.util.UUID

/**
 * Created by gma on 5/06/2014.
 */

class Sender(channel: Channel, queue: String) extends Actor
  with ActorLogging
  with StatsdLogging {

  def receive: Receive = {
    case m: Message => {
      val str = format(m)
      log.debug(str)
      measure.time("rabbit.time.ms") {
        channel.basicPublish("", queue, null, str.getBytes())
      }
      sender() ! true
    }
  }

  def format(m: Message): String = {
    new java.lang.StringBuilder(64)
      .append(String.format("{\"index\":{\"_index\":\"%s\",\"_type\":\"%s\",\"_id\":\"%s\"}}\n", m.index, m.msgType, m.id))
      .append(String.format("{\"EventId\":\"%s\",\"Timestamp\":\"%s\",\"Type\":\"%s\",\"Data\":\"%s\"}", m.id, m.data.timestamp, m.msgType, m.data.data.toString()))
      .append("\n").toString
  }
}
