package com.seek.akkahttp

import akka.actor.{ActorSelection}

/**
 * Created by maz on 6/6/2014.
 */
case class TimeMesurement(name:String, duration: Long)
case class CountMesurement(name: String)

class Measure(pool: ActorSelection) {
  var countName: String = null
  var timeName: String = null

  def count(s: String) = {
    countName = s
    this
  }

  def time(s: String) = {
    timeName = s
    this
  }

  def apply[T](f: => T): T = {
    val startTime = System.currentTimeMillis()
    val retVal: T = f

    if (timeName != null) {
      pool ! TimeMesurement(timeName, System.currentTimeMillis() - startTime)
    }

    if (countName != null) {
      pool ! CountMesurement(countName)
    }

    retVal
  }


}
