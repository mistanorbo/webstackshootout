package com.seek.akkahttp

import akka.actor.{Props, ActorSystem}
import scala.concurrent.duration._
import scala.util.Properties.{ lineSeparator => newLine }
/**
 * Created by gma on 5/06/2014.
 */
object ServerApp extends App {

  val system = ActorSystem("akkahttp")

  val settings = Settings(system)

  val timeout = Duration(5000, MILLISECONDS)

  val senderPool = system.actorOf(Props(new SenderPool(RabbitSetting(settings.rabbitHost, settings.rabbitUser, settings.rabbitPass, settings.rabbitQueue), 50)))

  val statsdPool = system.actorOf(Props(new StatsdPool(settings.statsdPrefix, settings.statsdHost, settings.statsdPort, 20)), "statsdPool")
  
  system.actorOf(Service.props(Settings(system).hostname, Settings(system).port, timeout, senderPool, statsdPool), "akka-service")

  system.awaitTermination()
}
