package com.seek.akkahttp

/**
 * Created by gma on 5/06/2014.
 */

case class RabbitSetting(host: String, username: String, password: String, queue: String)
