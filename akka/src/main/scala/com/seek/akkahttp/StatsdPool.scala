package com.seek.akkahttp

import akka.actor.{Props, Terminated, Actor, ActorRef}
import akka.routing.{RoundRobinRoutingLogic, Router, ActorRefRoutee}
import com.timgroup.statsd.NonBlockingStatsDClient

/**
 * Created by maz on 6/6/2014.
 */
class StatsdPool(prefix: String, host: String, port: Int, initSize: Int) extends Actor {
  var router = {
    val routees = Vector.fill(initSize) {
      ActorRefRoutee(createSender)
    }
    Router(RoundRobinRoutingLogic(), routees)
  }

  override def receive: Receive = {
    case m: CountMesurement =>
      router.route(m, sender())
    case m: TimeMesurement =>
      router.route(m, sender())
    case Terminated(a) =>
      router = router.removeRoutee(a).addRoutee(createSender)
  }

  private def createSender : ActorRef = {
    val statsd = new NonBlockingStatsDClient(prefix, host, port);
    context watch context.actorOf(Props(new StatsdSender(statsd)))
  }
}
