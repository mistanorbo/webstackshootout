package com.seek.akkahttp

import akka.actor.{Actor, ActorLogging}
import com.timgroup.statsd.StatsDClient
import akka.actor.Actor.Receive

/**
 * Created by maz on 6/6/2014.
 */
class StatsdSender(client: StatsDClient) extends Actor
  with ActorLogging {
  override def receive: Receive = {
    case m: TimeMesurement =>
      log.debug("Send {} to statsd", m.name)
      client.recordExecutionTime(m.name, m.duration.toInt)
    case m: CountMesurement =>
      log.debug("Send {} to statsd", m.name)
      client.incrementCounter(m.name)
  }
}
