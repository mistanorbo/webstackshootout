package com.seek.akkahttp

import akka.actor.{ActorRef, Terminated, Props, Actor}

import com.rabbitmq.client._
import akka.routing.{Router, RoundRobinRoutingLogic, ActorRefRoutee}
import java.util.UUID

/**
 * Created by maz on 6/5/2014.
 */
case class Message(index: String, msgType: String, id: UUID, data: ApiInput)

class SenderPool(setting: RabbitSetting, initSize: Int = 5) extends Actor {

  val connection = {
    val factory = new ConnectionFactory()
    factory.setHost(setting.host)
    factory.setUsername(setting.username)
    factory.setPassword(setting.password)
    factory.newConnection()
  }

  var router = {
    val routees = Vector.fill(initSize) {
      ActorRefRoutee(createSender)
    }
    Router(RoundRobinRoutingLogic(), routees)
  }

  override def receive: Receive = {
    case m: Message =>
      router.route(m, sender())
    case Terminated(a) =>
      router = router.removeRoutee(a).addRoutee(createSender)
      sender() ! new Error()
  }

  private def createSender : ActorRef = {
    val channel = connection.createChannel()
    channel.queueDeclare(setting.queue, true, false, false, null)

    context watch context.actorOf(Props(new Sender(channel, setting.queue)))
  }
}
