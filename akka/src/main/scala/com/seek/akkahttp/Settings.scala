package com.seek.akkahttp

import akka.actor.{ ExtendedActorSystem, Extension, ExtensionKey }
import scala.concurrent.duration.{ Duration, FiniteDuration, MILLISECONDS }

/**
 * Created by maz on 6/5/2014.
 */

object Settings extends ExtensionKey[Settings]

class Settings(system: ExtendedActorSystem) extends Extension {

  val hostname: String =
    system.settings.config getString "akkahttp.hostname"

  val port: Int =
    system.settings.config getInt "akkahttp.port"

  val timeout: FiniteDuration =
    Duration(system.settings.config.getDuration("akkahttp.timeout", MILLISECONDS), MILLISECONDS)

  val rabbitHost: String =
    system.settings.config getString "akkahttp.rabbitHost"

  val rabbitUser: String =
    system.settings.config getString "akkahttp.rabbitUser"

  val rabbitPass: String =
    system.settings.config getString "akkahttp.rabbitPass"

  val rabbitQueue: String =
    system.settings.config getString "akkahttp.rabbitQueue"

  val statsdPrefix: String =
    system.settings.config getString "akkahttp.statsdPrefix"

  val statsdHost: String =
    system.settings.config getString "akkahttp.statsdHost"

  val statsdPort: Int =
    system.settings.config getInt "akkahttp.statsdPort"
}
