package com.seek.akkahttp

import spray.json.{JsValue, DefaultJsonProtocol}

object ApiInput extends DefaultJsonProtocol {
  implicit val format = jsonFormat2(apply)
}

case class ApiInput(timestamp:String, data:JsValue)
