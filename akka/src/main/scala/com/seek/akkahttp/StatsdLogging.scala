package com.seek.akkahttp

import akka.actor.Actor

/**
 * Created by maz on 6/6/2014.
 */
trait StatsdLogging {this: Actor ⇒
  def measure = {
    new Measure(context.system.actorSelection("/user/statsdPool"))
  }
}
