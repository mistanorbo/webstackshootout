package com.seek.akkahttp

import scala.concurrent.duration._
import akka.actor.{ActorRef, ActorLogging, Props}
import akka.pattern.ask
import spray.routing._
import spray.httpx.SprayJsonSupport
import spray.http.StatusCodes
import spray.can.Http
import akka.io.IO
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by gma on 5/06/2014.
*/

object Service {

  def props(hostname: String, port: Int, timeout: FiniteDuration, senderPool: ActorRef, statsdPool: ActorRef): Props =
    Props(new Service(hostname, port, timeout, senderPool))
}

class Service(hostname: String, port: Int, timeout: FiniteDuration, senderPool: ActorRef) extends HttpServiceActor
  with ActorLogging
  with StatsdLogging
  with SprayJsonSupport {

  IO(Http)(context.system) ! Http.Bind(self, hostname, port)

  override def receive: Receive = {
    runRoute(apiRoute)
  }

  def apiRoute: Route =
    pathPrefix("events") {
      path(Segment / JavaUUID) { (evtType, id) =>
        get {
          complete(String.format("{\"type\":\"%s\",\"id\":\"%s\"}", evtType, id))
        } ~
          put {
            measure.count("api.counter").time("api.time.ms") {
              entity(as[ApiInput]) { input =>
                ctx =>
                  log.debug("Type: {}, ID: {}, Timestamp: {}, Data: {}", evtType, id, input.timestamp, input.data)
                  val future = senderPool.ask(Message("index", evtType, id, input))(timeout)
                  future map {
                    case e: Error => ctx.complete(StatusCodes.ServiceUnavailable)
                    case _ => ctx.complete(String.format("{\"Location\":\"/events/%s/%s\"}", evtType, id))
                  }
              }
            }
          }
      }
    }
}
