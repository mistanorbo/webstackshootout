(ns shootout.sharedconn (:gen-class)
  (:use compojure.core
        ring.middleware.json
        org.httpkit.server
        [clojure.tools.cli :only [cli]]
        ring.util.response)
  (:require [compojure.handler :as handler]
            [clj-statsd :as statsd]
            [compojure.route :as route]
            [langohr.core :as rmq]
            [langohr.channel :as lch]
            [langohr.basic :as lb]
            [clj-time.core :as timecore]
            [clj-time.format :as timeformat]))

(defn build-message [event-type id payload]
  (let [datetime (timeformat/parse (get payload :timestamp))
        metadata {:_id id
                  :_type event-type
                  :_index (str "seekedc-" (timeformat/unparse (timeformat/formatter "yyyyMMdd") datetime))}
        event {:EventId id
               :Timestamp (get payload :timestamp)
               :Type event-type
               :Data (get payload :data)}]
    (str metadata "\n" event "\n")))

(declare connected-put-event)

(defn put-event [conn event-type id payload]
  (let [start-ms (System/currentTimeMillis)
        channel (lch/open conn)
        qname "clojure"
        exchange "seekedc"
        message (build-message event-type id payload)
        rabbit-ms (System/currentTimeMillis)
        ]
    (statsd/increment :clojure.api.counter)
    (lb/publish channel exchange qname message :content-type "application/json" :type "shootout")
    (statsd/timing :clojure.rabbit.time (- (System/currentTimeMillis) rabbit-ms))
    (statsd/timing :clojure.api.time (- (System/currentTimeMillis) start-ms))
    (response {:location (str "events/" event-type "/" id)})))

;; Define route handlers
(defroutes app-routes
  (context "/events" []
    (GET "/" [] "Testing 123")
    (PUT "/:event-type/:id" {payload :body, {event-type :event-type id :id} :params}
         (connected-put-event event-type id payload))
    (route/not-found "Not Found")))

(defn start-server [{:keys [port workers]}]
  ;; Format request and response as JSON
  (let [handler (-> app-routes wrap-json-body wrap-json-response)
        conn (rmq/connect {:host "172.21.200.62" :username "elasticsearch" :password "Summer88"})]
    (statsd/setup "stats-server" 8125)
    ;; Partially apply the put-event function so we can reuse the RabbitMQ connection
    (def connected-put-event (fn [event-type id payload] (put-event conn event-type id payload)))
    (run-server handler {:port port :thread workers})
    (println (str "http-kit server listens with " workers " workers on port " port))))

(defn -main [& args]
  (let [default-workers (* (.availableProcessors (Runtime/getRuntime)) 2)
    [options _ banner]
    (cli args ["-p" "--port" "Port to listen" :default 8080 :parse-fn #(Integer/parseInt %)]
              ["-w" "--workers" "Number of worker threads" :default default-workers :parse-fn #(Integer/parseInt %)]
              ["--[no-]help" "Print this help"])]
    (when (:help options) (println banner) (System/exit 0))
    (start-server options)))
