(ns shootout.core (:gen-class)
  (:use compojure.core
        ring.middleware.json
        org.httpkit.server
        [clojure.tools.cli :only [cli]]
        ring.util.response)
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [clj-statsd :as statsd]
            [langohr.core :as rmq]
            [langohr.channel :as lch]
            [langohr.basic :as lb]))

;; Define route handlers
(defroutes app-routes
  (context "/events" []
    (GET "/" [] "Testing 123")
    (POST "/:event-type" {data :body, {event-type :event-type} :params}
      (let [start-ms (System/currentTimeMillis)
            conn (rmq/connect {:host "172.21.200.62" :username "elasticsearch" :password "Summer88"})
            channel (lch/open conn)
            qname "clojure"
            exchange "seekedc"
            event {:type event-type :data data}]
        (lb/publish channel exchange qname (str event) :content-type "application/json" :type "shootout")
        (rmq/close channel)
        (rmq/close conn)
        (statsd/timing :clojure.timing.rabbit.ms (- (System/currentTimeMillis) start-ms))
        (response "OK")
        ))
    (route/not-found "Not Found")))

(defn start-server [{:keys [port workers]}]
  (statsd/setup "stats-server" 8125)
  ;; Format responses as JSON
  (let [handler (-> app-routes wrap-json-body)]
    (run-server handler {:port port :thread workers})
    (println (str "http-kit server listens with " workers " workers on port " port))))

(defn -main [& args]
  (let [default-workers (* (.availableProcessors (Runtime/getRuntime)) 2)
    [options _ banner]
    (cli args ["-p" "--port" "Port to listen" :default 8080 :parse-fn #(Integer/parseInt %)]
              ["-w" "--workers" "Number of worker threads" :default default-workers :parse-fn #(Integer/parseInt %)]
              ["--[no-]help" "Print this help"])]
    (when (:help options) (println banner) (System/exit 0))
    (start-server options)))


