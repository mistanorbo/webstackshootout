package seekevents
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.core.check._
import io.gatling.http.Headers.Names._
import io.gatling.core.result._
import io.gatling.core.session._
import scala.concurrent.duration._
import bootstrap._
import assertions._

class SEEKEventsAPI_Put extends Simulation {

	def uuidInput = java.util.UUID.randomUUID.toString
	def dateTime = java.lang.String.format("%tFT%<tTZ",  new java.util.Date())
	
	var savedCount = 0
	var expectedCount = 0
	
	val putUrl = "http://apihost/events/" + System.getenv("WEBSHOOTOUT") + "/"
	val putRepeat = 1000
	val putUsers = 50
	val totalPutRequests = putRepeat * putUsers

	val statsUrl = "http://172.21.200.54:9200/seek-2014.4.30/_stats"
	//val statsRepeat = 5
    val statsPause = 30 // seconds
	val waitFor = 0// seconds
	
	val httpProtocol = http
		.acceptCharsetHeader("ISO-8859-1,utf-8;q=0.7,*;q=0.7")
		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
		.disableFollowRedirect
		.extraInfoExtractor((status, session, _, _) => status match {
			case _ => List(dateTime +";  events after: " + session("countAfter").validate[String]  + "; events before: " + session("countBefore").validate[String] + "; indexTime: " + session("indexTimeAfter").validate[String] + "; expectedCount: " + expectedCount.toString)   
		})
	
	val headers_1 = Map(
		"Keep-Alive" -> "115",
		"Content-Type" -> "application/json")
		
	val jsonIn = """
	  {"timestamp": "2014-04-30T15:30:00",  
		"data": {"url": "http:\/\/api.seek.com.au\/v2\/credits\/1",
					 "method": "POST",
					 "status": 201,
					 "advertiserId": 1234567,
					 "start": "2014-04-21",
					 "end": "2014-06-01",
					 "userId": "43211234",				  
					 "email": "somename@thisemail.com.au"}}
		"""	
		
	//Scenarios
	val scnBefore = scenario ("Count events before run") 
	   .group("GetEventsBefore"){
		exec(
			http("count_before")
				.get(statsUrl)
				.check(status.is(200))
				.check(jsonPath("$..docs[0].count").saveAs("countBefore"))
				.check(jsonPath("$..index_time_in_millis[0]").saveAs("indexTimeBefore")))
		.exec((session : Session) => {
			val startDT = "start time:" + dateTime
			val countBefore = "events before: " + session("countBefore").as[String]
			val indexTimeBefore = "index time before (milliseconds): " +  session("indexTimeBefore").as[String]
			val before = startDT  + "; "+  countBefore + "; " + indexTimeBefore
			println(before)
			expectedCount = java.lang.Integer.parseInt(session("countBefore").as[String]) + totalPutRequests
			println("expectedCount: " + expectedCount.toString + "; savedCount: " + savedCount.toString)
			//session.set("startDT", startDT)
			session
		})
	}
	    	
	val scnPut = scenario ("Put Json docs")
		.repeat(putRepeat) {
		exec((session : Session) => {
			val url = putUrl + uuidInput
			val dtime = dateTime
			println(url)
			print(dtime)			
			session.set("url", url)
		})
		.exec(http("put_request_1")
				.put("${url}")
				.headers(headers_1)
				.body(StringBody(jsonIn))
				.check(status.is(200)))
	}	

	val scnAfter = scenario ("Count events after run") 
	   //.group("GetEventsAfter")
	   //.during(15 seconds){
	   //.repeat(statsRepeat){
		.asLongAs(session => savedCount  < expectedCount){
		pause(statsPause  seconds)
		.exec(
			http("count_after")
				.get("http://172.21.200.54:9200/seek-2014.4.30/_stats")
				.check(status.is(200))
				.check(jsonPath("$..docs[0].count").saveAs("countAfter"))
				.check(jsonPath("$..indexing[0].index_time_in_millis").saveAs("indexTimeAfter")))
		.exec((session : Session) => {
			val checkDT = "check time:" + dateTime
			val countAfter = "events after: " + session("countAfter").as[String]
			savedCount = java.lang.Integer.parseInt(session("countAfter").as[String])
			val indexTimeAfter = "index time after: " +  session("indexTimeAfter").as[String]
			val after = checkDT  + "; "+  countAfter + "; " + indexTimeAfter 
			println(after)
			//session.set("checkDT", checkDT)
			session
		})
	}

	setUp(//scnBefore.inject(atOnce(1 users)),
		scnPut.inject(atOnce(putUsers users))
		//,scnAfter.inject(nothingFor(waitFor seconds),atOnce(1 users))
		)
		.protocols(httpProtocol)
}
