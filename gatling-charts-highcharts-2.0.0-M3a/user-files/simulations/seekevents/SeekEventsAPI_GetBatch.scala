package seekevents
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.core.check._
import io.gatling.http.Headers.Names._
import io.gatling.core.result._
import io.gatling.core.result.message._
import io.gatling.core.session._
import scala.concurrent.duration._
import scala.tools.nsc.io._
import bootstrap._
import assertions._
//import scala.util.parsing.json._

class SEEKEventsAPI_GetBatch extends Simulation {

	val httpProtocol = http
		.baseURL("http://172.21.200.109/events")
		.acceptCharsetHeader("ISO-8859-1,utf-8;q=0.7,*;q=0.7")
		.acceptHeader("application/json;q=0.9,*/*;q=0.8")
		.disableFollowRedirect
		.extraInfoExtractor((status, session, _, _) => status match {
			// case OK => List(session("eventsReturned").as[Integer], session("responseSize").as[Integer])
			// case _ => Nil
			//case _ => List(session("numEvents").validate[String], session("respSize").validate[String])   
			case _ => List("events returned: " + session("eventsReturned").validate[Integer], "; response size: " + session("responseSize").validate[Integer] + " Bytes")   
		})
	
	val headers_1 = Map(
		"Keep-Alive" -> "115",
		"Content-Type" -> "application/json")
		
	//Scenario
 	val scn = scenario ("Get a batch of Json docs")
		//.repeat(1, "loopName"){
		.group("GetAndLog"){
		exec(http("get_request_1")
				.get("/JobSeeker?from=2014-03-23&to=2015-04-30&limit=10000")
				.headers(headers_1)
				.check(status.is(200))
				.check(jsonPath("$..Id").count.not(0).saveAs("eventsReturned"))
				.check(bodyString.transform(_.map(_.size)).not(0).saveAs("responseSize")))				
		.exec((session : Session) => { 
			val numEvents = "events returned: " + session("eventsReturned").validate[String]
			val respSize = "response size: " + session("responseSize").validate[String] + " Bytes"
			println(numEvents)  
			println(respSize)
			// session.set("numEvents",numEvents)
			// session.set("respSize",respSize)
			session
		})
	}	 
	
	setUp(scn.inject(atOnce(1 users)))
		.protocols(httpProtocol)
}