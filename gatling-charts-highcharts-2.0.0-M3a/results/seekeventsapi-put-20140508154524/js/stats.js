var stats = {
    type: "GROUP",
contents: {
"put-request-1-cc2f69564b8b7389b63910022a86d9d5": {
        type: "REQUEST",
        name: "put_request_1",
path: "put_request_1",
pathFormatted: "put-request-1-cc2f69564b8b7389b63910022a86d9d5",
stats: {
    "name": "put_request_1",
    "numberOfRequests": {
        "total": "30",
        "ok": "30",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "329",
        "ok": "329",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "867",
        "ok": "867",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 27,
        "percentage": 90
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 3,
        "percentage": 10
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    }
}
    }
},
name: "Global Information",
path: "",
pathFormatted: "missing-name-b06d1db11321396efb70c5c483b11923",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "30",
        "ok": "30",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "329",
        "ok": "329",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "867",
        "ok": "867",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 27,
        "percentage": 90
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 3,
        "percentage": 10
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
