import (
    "webstackshootout/beego" // webstackshootout/beego/src/events ?
    . "github.com/smartystreets/goconvey/convey"
    "testing"
)

// Testing this API
// see: http://www.goinggo.net/2013/12/sample-web-application-using-beego-and.html

func TestSend(t *testing.T) {
    request := struct {
        EventId string `json:"event_id"`
        SessionId string `json:"session_id"`
        UserId string `json:"user_id"`
        Data struct {
            JobId string `json:"job_id"`
            Coordinates []float64 `json:"coordinates"`
        } `json:"condition"`
    }{}

    r, _ := http.NewRequest("GET", "/events/jobSeeker/123", nil)
    w := httptest.NewRecorder()
    beego.BeeApp.Handlers.ServeHTTP(w, r)

    response := struct {
        EventId string `json:"event_id"`
        SessionId string `json:"session_id"`
        UserId string `json:"user_id"`
        Data struct {
            JobId string `json:"job_id"`
            Coordinates []float64 `json:"coordinates"`
        } `json:"condition"`
    }{}
    json.Unmarshal(w.Body.Bytes(), &response)

    Convey("Subject: Test Send Endpoint\n", t, func() {
        Convey("Status Code Should Be 200", func() {
            So(w.Code, ShouldEqual, 200)
        })
        Convey("The Result Should Not Be Empty", func() {
            So(w.Body.Len(), ShouldBeGreaterThan, 0)
        })
        Convey("There Should Be A Result For Station 42002", func() {
            So(response.StationId, ShouldEqual, "42002")
        })
    })
}