package main

import (
	"github.com/astaxie/beego"
	"github.com/streadway/amqp"
	"github.com/cactus/go-statsd-client/statsd"
	"time"
	"log"
	"fmt"
)

type wrapped func()

// from: https://coderwall.com/p/l7e3fq
func Timed(fn wrapped, key string) {
  // Record the time around a function
  start := time.Now()
  fn()
  elapsed := time.Since(start) / time.Millisecond
  fmt.Printf("Time: %dms, Key: %s\n", elapsed, key)
  
  // first create a client (TODO: Do this out of scope?)
  client, err := statsd.New("stats-server:8125", "go")
  // handle any errors
  if err != nil {
          log.Fatal(err)
  }
  // make sure to clean up
  defer client.Close()

  // Send a stat
  err = client.Timing(key, int64(elapsed), 1.0)

  // handle any errors
  if err != nil {
    log.Printf("Error sending metric: %+v", err)
  }  
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

func SendMessage(username, password, host string) {
  // This is the rabit stuff that we need to break out
	conn, err := amqp.Dial("amqp://" + username + ":" + password + "@" + host + ":5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	defer ch.Close()

	q, err := ch.QueueDeclare(
		"go",    // name
		true,    // durable
		false,   // delete when usused
		false,   // exclusive
		false,   // noWait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")
  
	// Try to access these variables which are declared in main
  body := "hello"
	err = ch.Publish(
		"seekedc",  // exchange
		q.Name,     // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType:     "text/plain",
			Body:            []byte(body),
		})
	failOnError(err, "Failed to publish a message")  
}

type MessageStruct struct {
	Message string `json:"message"`
}

type JsonController struct {
	beego.Controller
}


func (this *JsonController) Get() { 
  fn := func() {
    // TODO: Wire this up properly
    SendMessage("elasticsearch", "Summer88", "rabbit-server")
  }
  Timed(fn, "timing.rabbit.ms")  
  
  m := MessageStruct{"Hello, World!"}
  this.Data["json"] = &m
  this.ServeJson()  
}

func main() {
	//don't need this set, beego default set it
	//runtime.GOMAXPROCS(runtime.NumCPU())
	beego.RunMode = "prod"
	beego.Router("/events", &JsonController{})
	beego.Run()
}
