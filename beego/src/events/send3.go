package main

import (
	"github.com/astaxie/beego"
	//"github.com/astaxie/beego/config" // Look into this
	//"github.com/astaxie/beego/middleware" // For error handling
	"github.com/streadway/amqp"
	"github.com/cactus/go-statsd-client/statsd"
	"encoding/json"
	"os"
	"log"
	"fmt"
	"time"
)

// Can we refernece this directly?
// http://golang.org/pkg/time/#pkg-constants 
const RFC3339 = "2006-01-02T15:04:05Z"

type wrapped func()

// from: https://coderwall.com/p/l7e3fq
func Timed(fn wrapped, key string) {
  // Record the time around a function
  start := time.Now()
  fn()
  elapsed := time.Since(start) / time.Millisecond
  fmt.Printf("Time: %dms, Key: %s\n", elapsed, key)
  
  // first create a client (TODO: Do this out of scope?)
  client, err := statsd.New("stats-server:8125", "go")
  if err != nil {
  	log.Fatal(err)
  }
  // make sure to clean up
  defer client.Close()

  // Send a stat
  err = client.Timing(key + ".time", int64(elapsed), 1.0)
  err = client.Inc(key + ".counter", 1, 1/1000)
  if err != nil {
    log.Printf("Error sending metric: %+v", err)
  }  
}

/*********** START CONNECTION POOLING **************/
// see: http://www.ryanday.net/2012/09/12/golang-using-channels-for-a-connection-pool/

type InitFunction func() (interface{}, error)
 
type ConnectionPoolWrapper struct {
	size int
	conn chan interface{}
}
 
/**
 Call the init function size times. If the init function fails during any call, then
 the creation of the pool is considered a failure.
 We call the same function size times to make sure each connection shares the same
 state.
*/
func (p *ConnectionPoolWrapper) InitPool(size int, initfn InitFunction) error {
	// Create a buffered channel allowing size senders
	p.conn = make(chan interface{}, size)
	for x := 0; x < size; x++ {
		conn, err := initfn()
		if err != nil {
			return err
		}
 
		// If the init function succeeded, add the connection to the channel
		p.conn <- conn
	}
	p.size = size
	return nil
}
 
func (p *ConnectionPoolWrapper) GetConnection() interface{} {
	return <-p.conn
}
 
func (p *ConnectionPoolWrapper) ReleaseConnection(conn interface{}) {
	p.conn <- conn
}
 
var rabbitMqPool = &ConnectionPoolWrapper{}

/**
 This function creates a connection to the database. It shouldn't have to know anything
 about the pool, It will be called N times where N is the size of the requested pool.
*/
func initRabbitMqConnections() (interface{}, error) {
  rabbit, err := NewRabbitMq()
  if err != nil {
    return nil, err
  } 

  return rabbit, nil
}

/*********** END CONNECTION POOLING **************/

// TODO: Move this into a service class
type RabbitMq struct {
    Conn  *amqp.Connection
    Ch    *amqp.Channel
    Q     amqp.Queue
}

func NewRabbitMq() (*RabbitMq, error) {
	fmt.Printf("Connecting to env: %s\n", os.Getenv("AMQP_URL"))

	conn, err := amqp.Dial(os.Getenv("AMQP_URL"))
	if (err != nil) {
		fmt.Println("Failed to connect to RabbitMQ", err);
		return nil, err
	}

	ch, err := conn.Channel()
	if (err != nil) {
		fmt.Println("Failed to open a channel", err);
		return nil, err
	}

	q, err := ch.QueueDeclare(
		"go",    // name
		true,    // durable
		false,   // delete when usused
		false,   // exclusive
		false,   // noWait
		nil,     // arguments
	)
	if (err != nil) {
		fmt.Println("Failed to declare a queue", err);
		return nil, err
	}
	return &RabbitMq{conn, ch, q}, nil
}

func (r *RabbitMq) Close() {
	if (r.Conn != nil) {
		r.Conn.Close()
	}

	if (r.Ch != nil) {
		r.Ch.Close()
	}
}

func (r *RabbitMq) Publish(routingKey, body string) error {
	err := r.Ch.Publish(
		"seekedc",   // exchange
		routingKey,  // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType:     "text/plain",
			Body:            []byte(body),
		})
	return err
}

type EventRequest struct {
	Timestamp string `json:"timestamp"`
}

type EventResponse struct {
	Location string `json:"location"`
}

type JsonController struct {
	beego.Controller
}

// Look into how go works with json
// see: http://blog.golang.org/json-and-go

func (this *JsonController) Put() { 
	fn := func() {
		eventType := this.Ctx.Input.Param(":eventType")
		eventId := this.Ctx.Input.Param(":eventId")
		url := fmt.Sprintf("events/%s/%s", eventType, eventId)
	    
	    log.Printf("PUT %s", url)

	    req := this.Ctx.Request
		payload := make([]byte, req.ContentLength)
		_, err := this.Ctx.Request.Body.Read(payload)
		if (err != nil) {
	        fmt.Println("Unable to read the JSON request", err);
		}
	    var m EventRequest
	    err = json.Unmarshal(payload, &m)
	    if err != nil {
	        fmt.Println("Unable to unmarshall the JSON request", err);
			this.Abort("400")
	    }

	    // Format the message
		var t, _ = time.Parse(RFC3339, m.Timestamp)
		var index = "seekedc-" + t.Format("2006.01.02")
		msg := fmt.Sprintf(`{"index":{"_index":"%s","_type":"%s","_id": "%s" }}\n`,
			index, eventType, eventId, string(payload))

		// Grab a connection from the pool and type assert to our database type
		rabbitMq := rabbitMqPool.GetConnection().(*RabbitMq) 
		// When this function exits, release our connection back to the pool
		defer rabbitMqPool.ReleaseConnection(rabbitMq)

		fnRabbit := func() {
			err := rabbitMq.Publish(eventType, msg)
			if (err != nil) {
				fmt.Println("Unable to push message", err);
			}
		}
		Timed(fnRabbit, "rabbit")  

		// Return back basic data
		res := EventResponse{url}
		this.Data["json"] = &res
		this.ServeJson()
	}
	Timed(fn, "api")  
}

func main() {
	// Create a pool of 10 connections using the initRabbitMqConnections function
	fmt.Println("Creating a connection pool of 10 connections")
  	err := rabbitMqPool.InitPool(10, initRabbitMqConnections)
	if err != nil {
		fmt.Println("Unable to create RabbitMq Connection Pool", err)
		return
	}

	//don't need this set, beego default set it
	//runtime.GOMAXPROCS(runtime.NumCPU())
	beego.RunMode = "prod"
	beego.HttpPort = 80
	beego.Router("/events/:eventType/:eventId", &JsonController{}, "put:Put")
	beego.Run()
}
