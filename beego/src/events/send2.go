package main

import (
	"github.com/astaxie/beego"
	//"github.com/astaxie/beego/config" // Look into this
	//"github.com/astaxie/beego/middleware" // For error handling
	"github.com/streadway/amqp"
	"github.com/cactus/go-statsd-client/statsd"
	"encoding/json"
	"os"
	"log"
	"fmt"
	"time"
)

// Can we refernece this directly?
// http://golang.org/pkg/time/#pkg-constants 
const RFC3339 = "2006-01-02T15:04:05Z"

// Good read on error handling
// see: http://blog.golang.org/defer-panic-and-recover
func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}

type wrapped func()

// from: https://coderwall.com/p/l7e3fq
func Timed(fn wrapped, key string) {
  // Record the time around a function
  start := time.Now()
  fn()
  elapsed := time.Since(start) / time.Millisecond
  fmt.Printf("Time: %dms, Key: %s\n", elapsed, key)
  
  // first create a client (TODO: Do this out of scope?)
  client, err := statsd.New("stats-server:8125", "go")
  if err != nil {
          log.Fatal(err)
  }
  // make sure to clean up
  defer client.Close()

  // Send a stat
  err = client.Timing(key + ".time", int64(elapsed), 1.0)
  err = client.Inc(key + ".counter", 1, 1.0)
  if err != nil {
    log.Printf("Error sending metric: %+v", err)
  }  
}
// Look at using a go routine perhaps for this, or else how do we close?
// see: https://gobyexample.com/goroutines
var globalRabbitMq *RabbitMq

// TODO: Move this into a service class
type RabbitMq struct {
    Conn  *amqp.Connection
    Ch    *amqp.Channel
    Q     amqp.Queue
}

func NewRabbitMq() (*RabbitMq) {
	fmt.Printf("Connecting to env: %s\n", os.Getenv("AMQP_URL"))

	conn, err := amqp.Dial(os.Getenv("AMQP_URL"))
	failOnError(err, "Failed to connect to RabbitMQ")
	//defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	//defer ch.Close()

	q, err := ch.QueueDeclare(
		"go",    // name
		true,    // durable
		false,   // delete when usused
		false,   // exclusive
		false,   // noWait
		nil,     // arguments
	)
	failOnError(err, "Failed to declare a queue")

	return &RabbitMq{conn, ch, q}
}

func (r *RabbitMq) Close() {
	if (r.Conn != nil) {
		r.Conn.Close()
	}

	if (r.Ch != nil) {
		r.Ch.Close()
	}
}

func (r *RabbitMq) Publish(routingKey, body string) error {
	err := r.Ch.Publish(
		"seekedc",   // exchange
		routingKey,  // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType:     "text/plain",
			Body:            []byte(body),
		})
	return err
}

type EventRequest struct {
	Timestamp string `json:"timestamp"`
}

type EventResponse struct {
	Location string `json:"location"`
}

type JsonController struct {
	beego.Controller
	rabbitMq *RabbitMq
}

func (this *JsonController) Prepare() {
    if this.rabbitMq == nil {
        this.rabbitMq = globalRabbitMq
    }
}

// Look into how go works with json
// see: http://blog.golang.org/json-and-go

func (this *JsonController) Put() { 
	fn := func() {
		eventType := this.Ctx.Input.Param(":eventType")
		eventId := this.Ctx.Input.Param(":eventId")
		url := fmt.Sprintf("events/%s/%s", eventType, eventId)
	    
	    log.Printf("PUT %s", url)

	    req := this.Ctx.Request
		payload := make([]byte, req.ContentLength)
		_, err := this.Ctx.Request.Body.Read(payload)
		if (err != nil) {
	        fmt.Println("Unable to read the JSON request", err);
		}
	    var m EventRequest
	    err = json.Unmarshal(payload, &m)
	    if err != nil {
	        fmt.Println("Unable to unmarshall the JSON request", err);
			this.Abort("400")
	    }

		if (this.rabbitMq != nil) {
			var t, _ = time.Parse(RFC3339, m.Timestamp)
			var index = "seekedc-" + t.Format("2006.01.02")

			// TODO: Consider if we need to string format source?
			msg := fmt.Sprintf(
			`{"index":{"_index":"%s","_type":"%s","_id": "%s" }}\n`,
			index, eventType, eventId, string(payload))

			fnRabbit := func() {
				err := this.rabbitMq.Publish(eventType, msg)
				//failOnError(err, "Failed to publish a message") 
				if (err != nil) {
					// Try to restart rabbit // FUTURE: use connect poolling
					// see: http://www.ryanday.net/2012/09/12/golang-using-channels-for-a-connection-pool/
					fmt.Println("Unable to push message, reseting connection", err);
					globalRabbitMq = NewRabbitMq()
				}
			}
			Timed(fnRabbit, "rabbit")  
		}

		// Return back basic data
		res := EventResponse{url}
		this.Data["json"] = &res
		this.ServeJson()
	}
	Timed(fn, "api")  
}

func main() {
	globalRabbitMq = NewRabbitMq()
	defer globalRabbitMq.Close()

	//don't need this set, beego default set it
	//runtime.GOMAXPROCS(runtime.NumCPU())
	beego.RunMode = "prod"
	beego.HttpPort = 80 
	beego.Router("/events/:eventType/:eventId", &JsonController{}, "put:Put")
	beego.Run()
}
