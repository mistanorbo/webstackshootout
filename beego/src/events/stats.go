package main

import (
        "fmt"
        "log"
        "github.com/cactus/go-statsd-client/statsd"
        "time"
)

type wrapped func()

// from: https://coderwall.com/p/l7e3fq
func Timed(fn wrapped, key string) {
  // Record the time around a function
  start := time.Now()
  fn()
  elapsed := time.Since(start) / time.Millisecond
  fmt.Printf("Time: %dms, Key: %s\n", elapsed, key)
  
  // first create a client (TODO: Do this out of scope?)
  client, err := statsd.New("stats-server:8125", "go")
  // handle any errors
  if err != nil {
          log.Fatal(err)
  }
  // make sure to clean up
  defer client.Close()

  // Send a stat
  err = client.Timing(key, elapsed, 1.0)

  // handle any errors
  if err != nil {
    log.Printf("Error sending metric: %+v", err)
  }  
}

func main() {
  fn := func() {
    fmt.Println("Hello from wrapped function!")
    time.Sleep(time.Duration(1) * time.Second)
  }
  Timed(fn, "timing.rabbit.ms")  
}